//---------------------------------------------------scope-------------------------------------------------------

//1
// Create function "createCounter" to generate numbers. It should receive the start value, which by default is equal to 0.
// For example:
// const counter = createCounter(44);
// counter(); // 45
// counter(); // 46
// counter(); // 47

function createCounter(a) {
    return () => {
        return a++;
    }
}
const getCount = createCounter(47)

//2
// Create function "multiply" to multiple values as shown below.
// For example:
// multiply(2)(4)(6); // 48
// multiply(3)(3)(3); // 27 

function multiply(a) {
    return (b) => {
        return (c) => {
            return a * b * c;
        }
    }
}

//----------------------------------------------------classes------------------------------------------------------------
class Task {
    constructor(name) {
        this.taskName = name;
    }
}

class Guest {
    constructor(tasksArr) {
        this.role = "guest";
        this.tasksArray = tasksArr;
    }

    getTask(index) {
        return this.tasksArray[index];
    }
}

class User extends Guest {
    constructor(tasksArr) {
        super(tasksArr);
        this.role = "user";
    }

    createTask(name) {
        this.tasksArray.push(new Task(name));
    }
}


class Admin {
    constructor(guestsUsersArr) {
        this.guestsUsersArr = guestsUsersArr;
    }

    getArray() {
        return this.guestsUsersArr;
    }

    changeType(a) {
        this.guestsUsersArr[a].role = this.guestsUsersArr[a].role === "guest" ? "user" : "guest";
    }
}

//examples
const guest = new Guest(
    [
        new Task('task name 1'),
        new Task('task name 2'),
        new Task('task name 3'),
    ]
);

guest.getTask(0) // { name: 'task name 1' }
guest.getTask(2) // { name: 'task name 3' }
guest.createTask(new Task('task name 4')) // taskName is not defined, should not work

const user = new User(
    [
        new Task('task name 1'),
        new Task('task name 2'),
        new Task('task name 3'),
    ]
);

user.getTask(0) // { name: 'task name 1' }
user.getTask(2) // { name: 'task name 3' }
user.createTask(new Task('task name 4'))
user.getTask(3) // {name: 'task name 4'}

const admin = new Admin(
    [
        new Guest([]),
        new Guest([new Task('task name 1')]),
        new User([]),
        new User([new Task('task name 2')]),
    ]
);

admin.getArray(); // [Guest, Guest, User, User]
admin.changeType(1);
admin.getArray(); // [Guest, User, User, User]





//-----------------------------------------prototypes------------------------------------------------------

//1
//Implement functionality for a logger object. It should have 3 methods, log, getLog and clearLog it should be used as shown below:

class Logger {

    logArray = [];

    log(name) {
        this.logArray.push(name);
    }

    getLog() {
        return this.logArray;
    }

    clearLog() {
        this.logArray = [];
    }
}

const logger = new Logger();
logger.log('Event 1');
logger.log('Event 2');
logger.getLog(); // ['Event 1', 'Event 2']
logger.clearLog();
logger.getLog(); // []

//2
//Implement a new method of the array "shuffle" to shuffle arrays. It should be used as shown below

Array.prototype.shuffle = function shuffle() {
    let length = this.length;
    for (let i = 0; i < length; i++) {
        let n = Math.floor(Math.random() * 10) % length;
        let chage = this[i];
        this[i] = this[n];
        this[n] = chage;
    }
    return this;
}




//----------------------------------------------------Asynchronous-----------------------------------------------------------

//1
// Create three functions:
// The first function callback1 takes one parameter: data as an array.
// This function calculates the sum of the elements in the array.

// The second function callback2 takes one parameter: data as an array. 
// This function multiplies the elements of the array.

// The third function w takes two parameters: s as a string and callback as one of the previous two functions.
// This function transforms the string into an array in which each element is represented by the length of a word in the string, 
//and the function call the callback function from its second parameter as shown below:

function callback1(arr) {
    let sum = 0;

    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];
    }

    return sum;
}

function callback2(arr) {
    let product = 1;

    for (let i = 0; i < arr.length; i++) {
        product *= arr[i];
    }

    return product;
}

function w(s, callback) {
    let arr = s.split(" ");

    for (let i = 0; i < arr.length; i++) {
        arr[i] = arr[i].length;
    }

    return callback(arr);
}


//2
// Create the function mocker which will return defined data with 1 second delay.
// It might be helpful in Front-end development when there is a need to make sure that 
// your interface works well with data that you get asynchronously.
// The use of the function is shown below:

function mocker(usersArr) {
    return () => {
        return new Promise( resolve=> setTimeout(() => { return resolve(usersArr) }, 1000));

    }
}

//3
//Create the function summarize1 that receives promises and returns promise with sum of their values as shown below:

function summarize1(p1, p2) {
    return Promise.all([p1, p2]).then(values => {
        let sum = 0;
        for (let i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum;
    })
}

//4?
//Create the async function summarize2 that receives promises and returns promise with sum of their values as shown below:

async function summarize2(p1, p2) {
    let sum = 0;
    return new Promise(async () => {
        await promise1.then((value) => {
            console.log(sum)
            sum += value;
            console.log(sum)

        });
        await promise2.then((value) => {
            console.log(sum)
            sum += value
            console.log(sum)

        });
        return sum;
    })
}

//--------------------------------------------------------------------------ES6-------------------------------------------------------------------------

//1
//Is valid JSON
function isValidJSON(json){
    try{
        JSON.parse(json);
        return true
    } catch(e) {
        return false;
    }
}

//2
//Greeting

function greeting(object){
    return `Hello, my name is ${object.name} ${object.surname} and I am ${object.age} years old.`
}

//3
//Unique Numbers

function unique(array){
    let set = new Set();
    for(let i=0;i<array.length;i++){
        set.add(array[i]);
    }
    return set;
}

//4
//Lego Generator

function* generator(arr){
    let index = 0;
    while (index < arr.length)
    yield arr[index++];
}

